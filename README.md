r-view is a demonstration R viewer which can be used to view a histogram of tweet "created\_at" times for tweets collected by 
the ScraperWiki Twitter Search Tool.

The viewer is based on the `knitr` package for R, which generates reports in specified formats (HTML, PDF etc) 
from a source template file which contains R commands which are executed to generate content.
In this case we use Rhtml, rather than the alternative Markdown, which enables us to specify custom CSS and 
JavaScript to integrate with the ScraperWiki platform.

The update hook is used so that the R view is regenerated when the underlying dataset changes. 
A primitive post-install hook is used try to install R packages not already installed on the platform.

Code for interacting with the ScraperWiki platform is in the scraperwiki\_utils.R file this contains:

 1. a function to read the SQL endpoint URL which is dumped into the box by some JavaScript used in the Rhtml template. 
 2. a function to read the JSON output from the SQL endpoint - this is a little convoluted since R cannot natively use https, 
 and solutions to read https are different on Windows and Linux platforms.
 3. a function to convert imported JSON dataframes to a clean dataframe. The data structure returned by the 
 rjson library is comprised of lists of lists and requires reprocessing to the prefered vector based format.
 
Code for generating the view elements is in view-source.R, this means that the R code embedded in the Rhtml template
are simple function calls. 

The main plot is generated using the `ggplot2` library. There is some code, commented out which tries to guess at some 
good axis limits, but this turned out to be difficult/not useful so the tool simply defaults to showing the whole dataset.
The key difficulty here was in the wrangling of POSIX format times.