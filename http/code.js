function save_api_stub(){
  scraperwiki.exec('echo "' + scraperwiki.readSettings().target.url + '" > ~/tool/dataset_url.txt; ')
}

function run_once_install_packages(){
  scraperwiki.exec('run-one tool/runonce.R &> tool/log.txt &')
}

$(function(){
   save_api_stub();
   run_once_install_packages();
});